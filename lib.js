const mail = require('./mail');

// Testing numbers 
module.exports.absolute = function(number) {
  if (number > 0) return number; 
  if (number < 0) return -number; 
  return 0; 
}

// testing strings
module.exports.greet = function(name){
  return 'welcome ' + name;
}

// testing arrays
module.exports.currencies = function(){
  return ['IND', 'JAP', 'AUS'];
}

// testing object
module.exports.prodcuts = function(productId){
  return {id: productId, price: 10}
}

// testing exception for registering user
module.exports.registerUser = function(username){
  if (!username) {
    throw new Error('Username is required.');
  }
  return {id: new Date().getTime(), username: username}
}

