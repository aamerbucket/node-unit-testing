const lib = require('../lib') 

describe('absolute', ()=>{
    it('should return a positive number if input is positive', () => {
        const result = lib.absolute(1); // pass 1 as a input value
        expect(result).toBe(1); 
     });
     
     it('should return a positive number if input is negative', () => {
         const result = lib.absolute(-1); // pass -1 as a input value
         expect(result).toBe(1); 
      });
     
     it('should return a number 0, if input is sent as 0 ', () => {
         const result = lib.absolute(0); // pass 0 as a input value
         expect(result).toBe(0); 
      });     
});

describe('greet', ()=>{
    it('should return a greeting message', () => {
        const result = lib.greet('Aamersohail');
        // expect(result).toBe('Aamer') // this to specific
        expect(result).toMatch(/Aamer/);
        expect(result).toContain('sohail');
    });
});

describe('currencies', ()=>{
    it('should return the currencies which are specified in the given array!', ()=>{
    const result = lib.currencies();
    expect(result).toContain('IND');
    expect(result).toEqual(expect.arrayContaining(['IND', 'AUS', 'JAP']));
    });
});

describe('product', ()=>{
    it('should return the product id with the price', ()=>{
        const result = lib.prodcuts(1);
        // for getting the exact value of the object
        //expect(result).toEqual({id: 1, price: 10});

        // atleast some properties should be inside it
        expect(result).toMatchObject({id: 1, price: 10});

        // check data type value of the object
        expect(result).toHaveProperty('id', 1);
    });
});

describe('registerUser', ()=>{    
    it('should throw if username is falsy', ()=>{
       // check all possible exceptions
        const args = [null, undefined, NaN, '', 0, false];
        args.forEach(a => {
            expect(()=> {lib.registerUser(null)} ).toThrow();
        });
    });
    it('should return an user object if valid username is passed', ()=>{
        const result = lib.registerUser('Aamer');
        expect(result).toMatchObject( { username: 'Aamer' } );
        expect(result.id).toBeGreaterThan(0);  
    });
});